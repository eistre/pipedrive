package eu.eistre.pipedrive

import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import eu.eistre.pipedrive.data.Person

class PersonsAdapter internal constructor(private val mContext: Context, private val mCursor: Cursor) : RecyclerView.Adapter<PersonsAdapter.ViewHolder>() {

    inner class ViewHolder constructor(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val mPersonName: TextView
        val mPersonCompany: TextView
        val mPersonPicture: ImageView
        var personID: Int = -1

        init {
            view.setOnClickListener(this)
            mPersonName = view.findViewById<View>(R.id.tv_person_name) as TextView
            mPersonCompany = view.findViewById<View>(R.id.tv_person_company) as TextView
            mPersonPicture = view.findViewById<View>(R.id.iv_picture) as ImageView
        }

        override fun onClick(view: View) {
            val details = DetailsFragment()
            details.arguments = Bundle()
            details.arguments.putInt("id", personID)
            (mContext as PersonsActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.frame, details)
                    .addToBackStack("details")
                    .commit()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(mContext)
        val view = inflater.inflate(R.layout.list_item_person, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (!mCursor.moveToPosition(position)) return

        holder.personID = mCursor.getInt(mCursor.getColumnIndex(Person.Person.COLUMN_ID))

        holder.mPersonName.text = mCursor.getString(
                mCursor.getColumnIndex(Person.Person.COLUMN_NAME)
        )
        holder.mPersonCompany.text = mCursor.getString(
                mCursor.getColumnIndex(Person.Person.COLUMN_COMPANY)
        )
        if (!mCursor.getString(mCursor.getColumnIndex(Person.Person.COLUMN_PICTURE)).isNullOrBlank())
            Glide.with(mContext).load(mCursor.getString(
                    mCursor.getColumnIndex(Person.Person.COLUMN_PICTURE)
            )).into(holder.mPersonPicture)
        else holder.mPersonPicture.setImageResource(R.mipmap.pipedrive)
    }


    override fun getItemCount(): Int {
        return mCursor.count
    }
}
