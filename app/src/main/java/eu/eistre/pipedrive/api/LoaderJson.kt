package eu.eistre.pipedrive.api

import android.app.LoaderManager
import android.content.AsyncTaskLoader
import android.content.ContentValues
import android.content.Context
import android.os.Bundle
import android.support.v7.preference.PreferenceManager
import android.util.Log
import eu.eistre.pipedrive.PersonsActivity
import eu.eistre.pipedrive.data.Person
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class LoaderJson : AsyncTaskLoader<JSONArray> {
    internal var mParams: Bundle? = null

    companion object {
        /**
         * Runs specified loader
         * @param callback  The callback that handles the results
         * *
         * @param loaderManager The loader responsible for running our task
         * *
         * @param id Type of loader from PersonsActivity.kt (Default 10)
         */
        fun run(callback: LoaderManager.LoaderCallbacks<JSONArray>, loaderManager: LoaderManager, id: Int = 10, bundle: Bundle = Bundle()) {
            val loader = loaderManager.getLoader<JSONArray>(id)
            if (loader == null) {
                loaderManager.initLoader(id, bundle, callback).forceLoad()
            } else {
                loaderManager.restartLoader(id, bundle, callback).forceLoad()
            }
        }
    }

    constructor(context: Context) : super(context) {}
    constructor(context: Context, params: Bundle) : super(context) {
        mParams = params
    }

    /**
     * Background tasks that gets json response, saves it to database and passes data to callback
     * *
     * @return The full array that was loaded
     */
    override fun loadInBackground(): JSONArray? {
        try {
            return insertDataToDb(
                    getJsonArray(
                            NetworkUtils.getResponseFromHttpUrl(
                                    NetworkUtils.buildURL(id
                                            , PreferenceManager.getDefaultSharedPreferences(context).getString("api_key", "no-api-key-specified")
                                    )
                            )
                    )
            )
        } catch (e: Exception) {
            Log.e("LoaderJsonException", this.id.toString() + " : " + e.message)
        }
        return null
    }

    /**
     * Function that strips data portion from JSON string, checks for query success
     * *
     * @param jsonString Raw JSON data as string
     * *
     * @return The JSONArray data contents
     */
    @Throws(JSONException::class)
    fun getJsonArray(jsonString: String): JSONArray {
        val OWM_SUCCESS = "success"
        val OWM_DATA = "data"
        //TODO: Implement additional & related data
        //val OWM_ADDITIONAL = "additional_data"
        //val OWM_RELATED = "related_objects"

        val inputJson = JSONObject(jsonString)
        if (inputJson.has(OWM_SUCCESS)) {
            val errorCode = inputJson.getString(OWM_SUCCESS)
            when (errorCode) {
                "true" -> return inputJson.getJSONArray(OWM_DATA)
                "false" -> {
                    //Error message comes within data??
                    Log.i("JsonTools", "Unsuccessful request: " + inputJson.getString(OWM_DATA))
                    throw JSONException("JSON FAIL: " + inputJson.getString(OWM_DATA))
                }
                else -> {
                    Log.wtf("JsonTools", "FAIL: " + inputJson.getString(OWM_DATA))
                    throw JSONException("Unexpected JSON result")
                }
            }
        } else {
            Log.w("JsonTools", "Success field not found!")
            throw JSONException("Unable to parse JSON.")
        }
    }

    /**
     * Very iterational way of inserting data to database
     * *
     * @param data JSONArray that we are working with
     * *
     * @return Pipes back exactly the same data
     */
    private fun insertDataToDb(data: JSONArray): JSONArray? {
        //TODO This is not really a nice solution but it works :)
        for (i in 0..data.length() - 1) {
            val personObject = data.getJSONObject(i)
            if (personObject.has("id")) {
                val cvPerson = ContentValues()
                cvPerson.put("id", personObject.getString("id"))

                if (!personObject.isNull("name")) {
                    cvPerson.put("name", personObject.getString("name"))
                }

                if (!personObject.isNull("org_name")) {
                    cvPerson.put("org_name", personObject.getString("org_name"))
                }

                if (!personObject.isNull("picture_id")) {
                    //cheat and get just the first LQ picture
                    cvPerson.put("picture", personObject.getJSONObject("picture_id").getJSONObject("pictures").getString("128"))
                }
                PersonsActivity.dbHelper.writableDatabase.insert(Person.Person.TABLE_NAME, null, cvPerson)

                //DO THE PHONES
                val phoneArray = personObject.getJSONArray("phone")
                PersonsActivity.dbHelper.writableDatabase.execSQL(
                        "DELETE FROM " + Person.PersonPhone.TABLE_NAME + " WHERE person_id = " + personObject.getString("id") + ";"
                )

                for (j in 0..phoneArray.length() - 1) {
                    val cvPhone = ContentValues()
                    cvPhone.put("person_id", personObject.getString("id"))
                    cvPhone.put("label", phoneArray.getJSONObject(j).getString("label"))
                    cvPhone.put("value", phoneArray.getJSONObject(j).getString("value"))
                    PersonsActivity.dbHelper.writableDatabase.insert(Person.PersonPhone.TABLE_NAME, null, cvPhone)
                }

                //DO THE EMAILS
                val emailArray = personObject.getJSONArray("email")
                PersonsActivity.dbHelper.writableDatabase.execSQL(
                        "DELETE FROM " + Person.PersonEmail.TABLE_NAME + " WHERE person_id = " + personObject.getString("id") + ";"
                )

                for (j in 0..emailArray.length() - 1) {
                    val cvEmail = ContentValues()
                    cvEmail.put("person_id", personObject.getString("id"))
                    cvEmail.put("label", emailArray.getJSONObject(j).getString("label"))
                    cvEmail.put("value", emailArray.getJSONObject(j).getString("value"))
                    PersonsActivity.dbHelper.writableDatabase.insert(Person.PersonEmail.TABLE_NAME, null, cvEmail)
                }
            }
        }
        //just pass it back
        return data
    }

}

