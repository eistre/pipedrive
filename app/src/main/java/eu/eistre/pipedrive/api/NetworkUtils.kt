package eu.eistre.pipedrive.api

import android.net.Uri
import android.util.Log
import eu.eistre.pipedrive.PersonsActivity
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.UnknownHostException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.SignatureException
import java.util.*

object NetworkUtils {
    internal val URL_BASE = "https://api.pipedrive.com/v1"
    internal val QUERY_APIKEY = "api_token"
    internal val QUERY_PERSONS = "persons"

    /**
     * Builds URL addresses for specified loaders
     * @param id     Loader ID (Specified in PersonsActivity.kt)
     * *
     * @param key    The API key (could be coded in function really)
     * *
     * @return The URL
     */
    @Throws(Exception::class)
    fun buildURL(id: Int, key: String): URL {
        var builtUri: Uri = Uri.parse(URL_BASE)
        when (id) {
            PersonsActivity.LOADER_ID_PERSONS //10
            -> builtUri = builtUri.buildUpon()
                    .appendPath(QUERY_PERSONS).build()
        //TODO: Implement person loader by user ID (from Bundle)
            PersonsActivity.LOADER_ID_PERSON //11
            -> builtUri = builtUri.buildUpon()
                    .appendPath(QUERY_PERSONS)
                    .appendPath("-1")
                    .build()
        }

        //TODO: Extra parameters
        /*if (params != null)
            for (param in params.keySet()) {
                builtUri = builtUri.buildUpon().appendQueryParameter(param, params.getString(param)).build()
            }
        */

        //Add API key
        builtUri = builtUri.buildUpon()
                .appendQueryParameter(QUERY_APIKEY, key)
                .build()

        try {
            Log.d("NetworkUtils.URL", "Built URI For ID: " + id + "::" + builtUri.toString())
            return URL(builtUri.toString())
        } catch (e: MalformedURLException) {
            Log.e("NetworkUtils", "Malformed URL: " + e.message)
            throw Exception("Malformed URL")
        }

    }

    /**
     * This method returns the entire result from the HTTP response.
     * @param url   The URL to fetch the HTTP response from.
     * *
     * @return  The contents of the HTTP response.
     * *
     * @throws  IOException Exceptions related to the data stream
     */
    @Throws(IOException::class)
    fun getResponseFromHttpUrl(url: URL): String {
        val urlConnection = url.openConnection() as HttpURLConnection
        urlConnection.requestMethod = "GET"
        //Log.v("NetworkUtils.get ", url.toString());
        try {
            try {
                val `in` = urlConnection.inputStream

                val scanner = Scanner(`in`)
                scanner.useDelimiter("\\A")

                val hasInput = scanner.hasNext()
                if (hasInput) {
                    val output = scanner.next()
                    scanner.close()
                    return output
                } else {
                    scanner.close()
                    throw IOException("NetworkUtils.get: No Input")
                }
            } catch (e: UnknownHostException) {
                Log.w("NO CONNECTION TO HOST", url.toString())
                throw IOException("NetworkUtils.get: Connection down")
            } catch (e: Exception) {
                Log.e("UNEXPECTED: ", e.toString())
                throw e
            } finally {
                urlConnection.disconnect()
            }
            //TODO: Rationalize error handling
        } catch (e: NoSuchAlgorithmException) {
            Log.e("NetworkUtils.get", e.toString())
            e.printStackTrace()
            throw IOException("NetworkUtils: No Such Algorithm exception")
        } catch (e: InvalidKeyException) {
            Log.e("NetworkUtils.get", e.toString())
            e.printStackTrace()
            throw IOException("NetworkUtils: Invalid Key")
        } catch (e: SignatureException) {
            Log.e("NetworkUtils.get", e.toString())
            e.printStackTrace()
            throw IOException("NetworkUtils: Invalid Signature")
        }

    }
}