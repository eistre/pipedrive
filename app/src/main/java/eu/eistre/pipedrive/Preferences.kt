package eu.eistre.pipedrive

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat

class Preferences : PreferenceFragmentCompat() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

    }
}