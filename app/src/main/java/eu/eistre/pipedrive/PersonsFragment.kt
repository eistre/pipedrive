package eu.eistre.pipedrive

import android.app.LoaderManager
import android.content.Loader
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import eu.eistre.pipedrive.api.LoaderJson
import eu.eistre.pipedrive.data.Person
import org.json.JSONArray

class PersonsFragment : Fragment(), LoaderManager.LoaderCallbacks<JSONArray> {


    //private val mProgress: ProgressBar by lazy { view!!.findViewById<View>(R.id.pb_persons_loading) as ProgressBar }
    //private val mRecyclerView: RecyclerView by lazy { view!!.findViewById<View>(R.id.rv_persons) as RecyclerView }
    private var mRecyclerAdapter: PersonsAdapter? = null
    private var mRecyclerView: RecyclerView? = null
    private var mProgress: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_persons, container, false)
    }

    override fun onResume() {
        super.onResume()
        mRecyclerView = view!!.findViewById<View>(R.id.rv_persons) as RecyclerView
        mProgress = view!!.findViewById<View>(R.id.pb_persons_loading) as ProgressBar

        mRecyclerView?.setHasFixedSize(true)
        mRecyclerView?.adapter = mRecyclerAdapter
        mRecyclerView?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        LoaderJson.run(this, activity.loaderManager)
    }

    override fun onCreateLoader(id: Int, args: Bundle): android.content.Loader<JSONArray> {
        mProgress?.visibility = View.VISIBLE
        return LoaderJson(context, args)
    }

    override fun onLoadFinished(loader: android.content.Loader<JSONArray>, data: JSONArray?) {
        mProgress?.visibility = View.GONE
        if (data == null) {
            Toast.makeText(context, "Can't connect to API, check preferences!", Toast.LENGTH_LONG).show()
        } else {
            mRecyclerAdapter = PersonsAdapter(context,PersonsActivity.dbHelper.readableDatabase.query(
                    Person.Person.TABLE_NAME, null, null, null, null, null, null)
            )
            mRecyclerView?.adapter = mRecyclerAdapter
        }
    }

    override fun onLoaderReset(p0: Loader<JSONArray>?) {
        //All good, just reset :)
    }
}