package eu.eistre.pipedrive

import android.app.Activity
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import eu.eistre.pipedrive.data.DBHelper

class PersonsActivity : AppCompatActivity() {
    private val mToolbar: Toolbar by bind(R.id.toolbar)
    private val mPersons = PersonsFragment()
    private val mPreferences = Preferences()

    companion object {
        lateinit var dbHelper: DBHelper
        val LOADER_ID_PERSONS = 10
        val LOADER_ID_PERSON = 11

    }

    fun <T : View> Activity.bind(@IdRes res: Int): Lazy<T> {
        @Suppress("UNCHECKED_CAST")
        return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_persons)
        setSupportActionBar(mToolbar)
        dbHelper = DBHelper(applicationContext)
        supportFragmentManager.beginTransaction().replace(R.id.frame, mPersons).commit()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.persons, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.action_settings -> {
                supportFragmentManager.beginTransaction().
                        replace(R.id.frame, mPreferences).
                        addToBackStack("settings").
                        commit()
                return true
            }
            R.id.action_home -> {
                supportFragmentManager.beginTransaction().
                        replace(R.id.frame, mPersons).
                        addToBackStack("persons").
                        commit()
                return true
            }
            else ->
                return super.onOptionsItemSelected(item)
        }
    }


}
