package eu.eistre.pipedrive

import android.database.Cursor
import android.database.DatabaseUtils
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import eu.eistre.pipedrive.data.Person

class DetailsFragment : Fragment() {
    private var UserID: String? = null
    lateinit var person: Cursor
    lateinit var phone: Cursor
    lateinit var email: Cursor
    private val mName: TextView by lazy { view!!.findViewById<View>(R.id.tv_person_name) as TextView }
    private val mCompany: TextView by lazy { view!!.findViewById<View>(R.id.tv_person_company) as TextView }
    private val mContact: TextView by lazy { view!!.findViewById<View>(R.id.tv_contact) as TextView }
    private val mPicture: ImageView by lazy { view!!.findViewById<View>(R.id.iv_picture) as ImageView }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_persons_details, container, false)
    }

    override fun onResume() {
        super.onResume()
        Log.v("Details opened", "User ID: " + arguments.getInt("id"))

        //TODO this whole thing might look better with proper objects
        person = PersonsActivity.dbHelper.readableDatabase.query(
                Person.Person.TABLE_NAME,
                null,
                "id=" + arguments.getInt("id"),
                null, null, null, null)
        DatabaseUtils.dumpCursor(person)
        if (person.moveToFirst()) {

            mName.setText(person.getString(person.getColumnIndex(Person.Person.COLUMN_NAME)))
            mCompany.setText(person.getString(person.getColumnIndex(Person.Person.COLUMN_COMPANY)))

            if (!person.getString(person.getColumnIndex(Person.Person.COLUMN_PICTURE)).isNullOrBlank())
                Glide.with(context).load(
                        person.getString(person.getColumnIndex(Person.Person.COLUMN_PICTURE))
                ).into(mPicture)

            phone = PersonsActivity.dbHelper.readableDatabase.query(
                    Person.PersonPhone.TABLE_NAME,
                    null,
                    "person_id=" + arguments.getInt("id"),
                    null, null, null, null)
            while (phone.moveToNext()) {
                mContact.append("Phone: " +
                        phone.getString(phone.getColumnIndex(Person.PersonPhone.COLUMN_VALUE)) + " (" +
                        phone.getString(phone.getColumnIndex(Person.PersonPhone.COLUMN_LABEL)) + ")\n")
            }

            email = PersonsActivity.dbHelper.readableDatabase.query(
                    Person.PersonEmail.TABLE_NAME,
                    null,
                    "person_id=" + arguments.getInt("id"),
                    null, null, null, null)
            while (email.moveToNext()) {
                mContact.append("E-mail: " +
                        email.getString(email.getColumnIndex(Person.PersonEmail.COLUMN_VALUE)) + " (" +
                        email.getString(email.getColumnIndex(Person.PersonEmail.COLUMN_LABEL)) + ")\n")
            }

        } else Toast.makeText(context, "User information not found!", Toast.LENGTH_LONG).show()
    }
}