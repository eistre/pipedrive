package eu.eistre.pipedrive.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        val DATABASE_NAME = "database.db"
        private val DATABASE_VERSION = 8
    }

    override fun onCreate(db: SQLiteDatabase) {
        createTables(db)
    }

    fun doReset(db: SQLiteDatabase) {
        db.execSQL("DROP TABLE IF EXISTS " + Person.Person.TABLE_NAME)
        db.execSQL("DROP TABLE IF EXISTS " + Person.PersonPhone.TABLE_NAME)
        db.execSQL("DROP TABLE IF EXISTS " + Person.PersonEmail.TABLE_NAME)
        createTables(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //TODO Make real database upgrade
        doReset(db)
    }

    fun createTables(db: SQLiteDatabase) {
        var SQL_CREATE_TABLE = "CREATE TABLE " +
                Person.Person.TABLE_NAME + " (" +
                Person.Person.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                Person.Person.COLUMN_NAME + " TEXT NOT NULL, " +
                Person.Person.COLUMN_COMPANY + " TEXT," +
                Person.Person.COLUMN_PICTURE + " TEXT" +
                ");"
        db.execSQL(SQL_CREATE_TABLE)

        //TODO Set up foreign keys for person_id for the next two tables
        SQL_CREATE_TABLE = "CREATE TABLE " +
                Person.PersonPhone.TABLE_NAME + " (" +
                "person_id INTEGER NOT NULL, " + //Make this into foreign key
                Person.PersonPhone.COLUMN_LABEL + " TEXT, " +
                Person.PersonPhone.COLUMN_VALUE + " TEXT " +
                ");"
        db.execSQL(SQL_CREATE_TABLE)

        SQL_CREATE_TABLE = "CREATE TABLE " +
                Person.PersonEmail.TABLE_NAME + " (" +
                "person_id INTEGER NOT NULL, " + //Make this into foreign key
                Person.PersonEmail.COLUMN_LABEL + " TEXT, " +
                Person.PersonEmail.COLUMN_VALUE + " TEXT " +
                ");"
        db.execSQL(SQL_CREATE_TABLE)
    }

    /* TODO This would be a smarter way of inserting data in bulk instead of single inserts :)
    fun addBulk(list: List<ContentValues>, insertDB: String) {
        val db = this.getWritableDatabase()
        db.beginTransaction()
        try {
            for (cv in list) {
                db.insert(insertDB, null, cv)
            }
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
        db.close()
    }*/

}
