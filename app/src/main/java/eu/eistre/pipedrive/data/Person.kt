package eu.eistre.pipedrive.data

import android.provider.BaseColumns

object Person {
    class Person : BaseColumns {
        companion object {
            val TABLE_NAME = "Persons"
            val COLUMN_ID = "id"
            val COLUMN_NAME = "name"
            val COLUMN_COMPANY = "org_name"
            val COLUMN_PICTURE = "picture"
        }
    }

    class PersonPhone : BaseColumns {
        companion object {
            val TABLE_NAME = "Phones"
            val COLUMN_VALUE = "value"
            val COLUMN_LABEL = "label"
        }
    }

    class PersonEmail : BaseColumns {
        companion object {
            val TABLE_NAME = "Emails"
            val COLUMN_VALUE = "value"
            val COLUMN_LABEL = "label"
        }
    }

    //TODO A better way would be to use this and gson
    class PersonObj {
        private var id: Int = -1
        private var name: String? = null
        private var company: String? = null
        private lateinit var phones: List<Phone>
        private lateinit var emails: List<Email>

        internal inner class Phone {
            var type: String? = null
            var value: String? = null
        }

        internal inner class Email {
            var type: String? = null
            var value: String? = null
        }
    }
}
